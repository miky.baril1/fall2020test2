package employees;

public class PayrollManagement {

	public static void main(String[] args) {
		//Initialize 5 different employees
		Employee[] emp = new Employee [5];
		emp[0]= new SalariedEmployee(40000);
		emp[1]= new HourlyEmployee(40,32);
		emp[2]= new UnionizedHourlyEmployee(40, 32, 45, 1.5);
		emp[3]= new SalariedEmployee(55000);
		emp[4]= new HourlyEmployee(50,40);
		
		System.out.println(getTotalExpenses(emp)+"$ of total expenses");
		
	}
	//Running through for loop to print every employees weekly salary together
	public static double getTotalExpenses(Employee[] emp) {
		double weeklyExpenses = 0;
		for(int i = 0; i<emp.length;i++) {
		 weeklyExpenses = weeklyExpenses+emp[i].getWeeklyPay(0);
	}
		return weeklyExpenses;
}
}
