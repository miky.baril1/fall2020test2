package employees;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	protected double maxHoursPerWeek;
	protected double overtimeRate;
	//Super Constructor inherited from HourlyEmployee
	
	public UnionizedHourlyEmployee(double hoursWeekly, double hourSal,double maxHoursPerWeek,double overtimeRate) 
	{
		super(hoursWeekly, hourSal);
	}
	
	public double getWeeklyPay(double totalPay) {
	if(hoursWeekly<=maxHoursPerWeek) {
		double weeklyPay = hoursWeekly*hourSal;
		
		return weeklyPay;
	}
		else {
			double maxPay = maxHoursPerWeek*hourSal;
			double weeklyOvertime = (hoursWeekly-maxHoursPerWeek)*overtimeRate;
			double totalWeeklyPay = maxPay+weeklyOvertime;
		
			return totalWeeklyPay;
		}
	
	}
}