package employees;

public class HourlyEmployee implements Employee {
	//Global variable for hoursWeekly
	
	protected double hoursWeekly;
	protected double hourSal;
	
	//Get method HOURS WEEKLY 
		public double getHoursWeekly() {
		
			return this.hoursWeekly;
		
		}
		
	//Get method HOUR SALARY
	public double getHourSal() {
		
		return this.hourSal;
		
	}		
	
	//Constructor/Setter	
	public HourlyEmployee(double hoursWeekly, double hourSal) {
			this.hoursWeekly=hoursWeekly;
			this.hourSal=hourSal;
		}
		
		//Interface Method
		@Override
		public double getWeeklyPay(double totalPay) {
			totalPay = hoursWeekly*hourSal;
			return totalPay;
		}
	

}
