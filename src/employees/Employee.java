package employees;

public interface Employee {
	
	//Method for all implementing classes
	double getWeeklyPay(double totalPay);
	
}
