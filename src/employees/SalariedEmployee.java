package employees;

public class SalariedEmployee implements Employee {
	
	//Creating global variable
	private double yearlySal;
	
	//Get method
	public double getYearlySal() {
	
		return this.yearlySal;
	
	}
	//Constructor/Setter
	public SalariedEmployee(double yearlySal) {
		
		this.yearlySal=yearlySal;
			
	}
	//Interface method
	@Override
	public double getWeeklyPay(double totalPay) {
		
		totalPay= (yearlySal/52);
		
		return totalPay;
	}
	
	
}
