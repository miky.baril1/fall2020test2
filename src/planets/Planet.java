import movies.importer.Movie;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return this.planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return this.order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return this.radius;
	}
	Planet earth = new Planet(planetarySystemName,name,order,radius);
	Planet mars = new Planet(planetarySystemName,name,order,radius);
	
	public int sortP(Planet planet) {
		//Array sorter
		int sorter = this.getName().compareTo(planet.getName());
		if(sorter == 1) {
			return (int) getRadius();
		}
	}
	@Override
	// equals method
	public boolean equals(Object p) {
		if(this==p) {
			return true;
		}
		else {
			return false;
		}
		
		if(this.getPlanetarySystemName() != ((Planet) p).getPlanetarySystemName()) 
		{
			return false;
		}
		
		if(this.name.equals(earth.name)) {
			return false;
		}
		return true;
}

@Override
//hashcode
public int hashCode() {
	String bothNames = this.name +this.planetarySystemName;
	return bothNames.hashCode();
}
}
